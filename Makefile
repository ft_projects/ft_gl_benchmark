# ------------------------------------------------------------------
# FT.Ltd,Co
# Author almpazel: 
# date : 2023-5-7
# this file is template for project configure and make option
# ------------------------------------------------------------------

FT_CC 			:= gcc
FT_AR 			:= ar
FT_DIR_APP		:= .
FT_DIR 			:= $(FT_DIR_APP)/ft
FT_CFLAGS 		:= -O0 -g -I.
FT_LFLAGS 		:= -lm
BUILD_DIR 		:= _build
FT_SRCS			:=


include $(FT_DIR)/config.mk
include app.mk
FT_SRCS 		+= ./main.c

FT_OBJS = $(addprefix $(BUILD_DIR)/,$(FT_SRCS:.c=.o))

all: env build

env:
	@echo "Building env......."
	@if [ ! -d "$(BUILD_DIR)" ]; then mkdir -p $(BUILD_DIR); fi

$(BUILD_DIR)/%.o: %.c
	@echo "Compiling $<"
	@mkdir -p $(BUILD_DIR)/$(dir $<)
	@$(FT_CC) $(FT_CFLAGS) -c $< -o $@

clean:
	rm -fr $(BUILD_DIR)


build:build.ft $(FT_OBJS)
	@echo "Linking $(FT_APP_BIN)"
	@$(FT_CC) -o $(FT_APP_BIN) $(FT_OBJS) $(FT_APP_LIB) $(FT_LFLAGS)
	@echo "Linking ..."
	@echo "Linking ..."
	@echo "Builded $(FT_APP_BIN)"

rebuild: clean build

build.ft:
	mkdir -p _build
	@cd ft && make -j && cd ..

clean.ft:
	@cd ft && make clean && cd ..

rebuild.ft: clean.ft build.ft
