# ------------------------------------------------------------------
# FT.Ltd,Co
# Author almpazel: 
# date : 2023-5-7
# this file is template for project configure and make option
# ------------------------------------------------------------------

FT_DIR_APP		:=.

FT_APP_TYPE		:= "dev"
FT_APP_BIN 		:= $(FT_DIR_APP)/assets/demo_ft
FT_APP_LIB 		:= $(FT_DIR)/_build/libft.a

FT_CFLAGS 		+= -D FT_APP_TYPE=$(FT_APP_TYPE)
FT_CFLAGS 		+= -I$(FT_DIR_APP)
FT_CFLAGS 		+= -I$(FT_DIR_APP)/src

FT_SRCS 		+= $(FT_DIR_APP)/app.c


include $(FT_DIR_APP)/assets/assets.mk
include $(FT_DIR_APP)/src/src.mk