#include "ft.h"

#ifndef _FT_APP_CONF_H
#define _FT_APP_CONF_H 1

#define APP_VERSION "1.3.7"
#define APP_FLAG_CREATE     0
#define APP_FLAG_EDIT       1
#define APP_FLAG_APPOINT    2
#define APP_FLAG_PROBE      3
#define APP_FLAG_MAGIC      4
#define APP_FLAG_MAGIC_EDIT      5


#endif //_FT_APP_CONF_H