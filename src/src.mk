# ------------------------------------------------------------------
# FT.Ltd,Co
# Author almpazel: 
# date : 2023-5-7
# this file is template for project configure and make option
# ------------------------------------------------------------------

FT_CFLAGS			+= -I$(FT_DIR_APP)/src
FT_SRCS += $(shell find -L $(FT_DIR_APP)/src -name \*.c)
