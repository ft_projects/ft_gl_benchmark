/**
 * @file ft_demo_benchmark.h
 *
 */

#define FT_USE_DEMO_BENCHMARK 1

#ifndef FT_DEMO_BENCHMARK_H
#define FT_DEMO_BENCHMARK_H

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "ft.h"

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/
typedef void finished_cb_t(void);


/**********************
 * GLOBAL PROTOTYPES
 **********************/
void ft_demo_benchmark(void);

void ft_demo_benchmark_run_scene(int_fast16_t scene_no);

void ft_demo_benchmark_set_finished_cb(finished_cb_t * finished_cb);

/**
 * Make the benchmark work at the highest frame rate
 * @param en true: highest frame rate; false: default frame rate
 */
void ft_demo_benchmark_set_max_speed(bool en);

/**********************
 *      MACROS
 **********************/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*FT_DEMO_BENCHMARK_H*/
