#include "app_ft.h"

static PortingT porting={};

static Any app_start(Any param){
    app_init();
}

int main(int argc, char const *argv[])
{
    //Todo start configures
    porting.argc=argc;
    porting.argv=argv;
    porting.appEntry=app_start;

    // porting.thread_model=app_model_thread;
    // porting.thread_wifi=biz_wifi_thread;
    // porting.thread_uart=fuart_thread;

    return portingInit(&porting);
}
